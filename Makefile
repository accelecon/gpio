all: gpio

gpio: gpio.c
	$(CC) $(CFLAGS) gpio.c -lrt -lpthread -o gpio

clean:
	@rm -f gpio *~

