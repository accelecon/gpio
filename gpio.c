//-----------------------------------------------------------------------------
// gpio.c
//-----------------------------------------------------------------------------

#define PROGRAM "gpio"
#define VERSION "1.208.07"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <getopt.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <errno.h>
#include <time.h>

// #define BUGGERS // enable for system logic debugging

//--------------------------------------------------------------------------------------------------
// GPIO mapped filenames.  The LED mapped filenames are generated dynamically in SetLeds().
//--------------------------------------------------------------------------------------------------

#ifdef GPIO_PLATFORM_NAMES

#define GPIO_ETH_EN   "/sys/class/gpio/pioA0/value"
#define GPIO_ETH_ACT  "/sys/class/gpio/pioA1/value"
#define GPIO_ETH_LINK "/sys/class/gpio/pioA2/value"

#define GPIO_USB_INT_EN "/sys/class/gpio/pioD4/value"
#define GPIO_USB_EXT_EN "/sys/class/gpio/pioD5/value"
#define GPIO_USB_INT_OC "/sys/class/gpio/pioD8/value"
#define GPIO_USB_EXT_OC "/sys/class/gpio/pioD9/value"

#else

#define GPIO_ETH_EN   "/sys/class/gpio/gpio32/value"
#define GPIO_ETH_ACT  "/sys/class/gpio/gpio33/value"
#define GPIO_ETH_LINK "/sys/class/gpio/gpio34/value"

#define GPIO_USB_INT_EN "/sys/class/gpio/gpio132/value"
#define GPIO_USB_EXT_EN "/sys/class/gpio/gpio133/value"
#define GPIO_USB_INT_OC "/sys/class/gpio/gpio136/value"
#define GPIO_USB_EXT_OC "/sys/class/gpio/gpio137/value"

#endif

//--------------------------------------------------------------------------------------------------
// There are three kinds of patterns:
//
//   1 - signal led patterns (--pat=cylon/download)
//   2 - usg/gen led patterns (--pat=reconfigure)
//   3 - gen led patterns (--pat=2y-, --pat=4ryr-)
//
// Pattern types 1 and 2/3 are independent.  Pattern type 2
// trumps pattern type 3.  Pattern type 1 is stored in the
// file PATTERN_SIG.  Pattern types 2 and 3 are stored in
// the file PATTERN_GEN.  The PATTERN_USB file is just a
// semaphore to indicate when the usb light is involved in
// a reconfigure (type 2) pattern.
//--------------------------------------------------------------------------------------------------

#define PATTERN_SIG "/tmp/pattern.sig"
#define PATTERN_GEN "/tmp/pattern.gen"
#define PATTERN_USB "/tmp/pattern.usb"

//--------------------------------------------------------------------------------------------------
// The most recent settings for --sig, --err, --usb and --gen
// are stored in the following files.  Non-existence is equivalent
// to '-' (off).  The --sig value is only active if the --err
// value is '-'.
//--------------------------------------------------------------------------------------------------

#define FILE_SIG "/tmp/gpio.sig"
#define FILE_ERR "/tmp/gpio.err"
#define FILE_USB "/tmp/gpio.usb"
#define FILE_GEN "/tmp/gpio.gen"

//--------------------------------------------------------------------------------------------------
// A few handy typedefs and defines.
//--------------------------------------------------------------------------------------------------

typedef       char *  cptr;
typedef const char * ccptr;

typedef uint8_t bool;

#define true  1
#define false 0

#define COLORS "wypragb-"

//--------------------------------------------------------------------------------------------------
// A handy globals.  Avoid re-entrant reuse!
//--------------------------------------------------------------------------------------------------

static FILE * f = NULL;

static int GlobalReturnCode = 0; // assume success

//--------------------------------------------------------------------------------------------------
// Create a pattern file.
//--------------------------------------------------------------------------------------------------

static void PatternInitiate( ccptr Filename) {
  if (f = fopen( Filename, "w")) {
    fprintf( f, "locked\n");
    fclose( f);
} }

//--------------------------------------------------------------------------------------------------
// Return true if a pattern file exists.
//--------------------------------------------------------------------------------------------------

static bool PatternActive( ccptr Filename) {
  if (f = fopen( Filename, "r")) {
    fclose( f);
    return true;
  }
  return false;
}

//--------------------------------------------------------------------------------------------------
// Remove a pattern file.
//--------------------------------------------------------------------------------------------------

static void PatternTerminate( ccptr Filename) {
  unlink( Filename);
}

//--------------------------------------------------------------------------------------------------
// Store a string value.
//--------------------------------------------------------------------------------------------------

static void PutString( ccptr Filename, ccptr Value) {
  if (f = fopen( Filename, "w")) {
    if (fprintf( f, "%s", Value) != strlen( Value)) {
      fprintf( stderr, "*** PutString: File '%s' write '%s' error!\n", Filename, Value);
      GlobalReturnCode = 11;
    }
    fclose( f);
  } else {
    fprintf( stderr, "*** PutString: File '%s' open for write failed!\n", Filename);
    GlobalReturnCode = 12;
} }

//--------------------------------------------------------------------------------------------------
// Read back a string value.  Return an empty string on error.
//--------------------------------------------------------------------------------------------------

static cptr GetString( ccptr Filename) {
  static char Value[16];
  memset( (void *) Value, 0, 16);
  if (f = fopen( Filename, "r")) {
    fread( Value, 1, 15, f);
    fclose( f);
  }
  return Value;
}

//--------------------------------------------------------------------------------------------------
// Set the output level of a gpio pin.
//--------------------------------------------------------------------------------------------------

static void SetGpio( ccptr Path, ccptr Value) {
  #ifdef BUGGERS
    printf( "SetGpio: '%s' = '%s'\n", Path, Value);
  #endif
  if (f = fopen( Path, "w")) {
    if (strcmp( Value, "1") ||  strcmp( Value, "0")) {
      fprintf( f, "%s", Value);
    } else {
      fprintf( stderr, "*** SetGpio: Unexpected value '%s' when setting enable pin!  Use 0 to disable, 1 to enable.\n", Value);
      GlobalReturnCode = 21;
    }
    fclose( f);
  } else {
    fprintf( stderr, "*** SetGpio: Could not open gpio sysfs interface!\n");
    GlobalReturnCode = 22;
} }

//--------------------------------------------------------------------------------------------------
// For each combination of type/index, set function to value, then delay.  When both type and index
// lists are specified, cycle each type within each index, and only delay between indexes.  The
// delay is specified by Stagger_ms in milliseconds.  When valuelist has more than one value, step
// through the values as needed.  The last (or only) value in valuelist is used thereafter.
//--------------------------------------------------------------------------------------------------

static void SetLeds( ccptr TypeList, ccptr IndexList, ccptr Function, ccptr ValueList, uint32_t Stagger_ms) {
  cptr x;
  char Path[64], Index[64], Type[64], Value[64];
  while (*IndexList) {
    ccptr TypeList2;
    strcpy( Index, IndexList);
    if (x = strchr( Index, ',')) {
      *x = 0;
      IndexList++;
    }
    IndexList += strlen( Index);
    TypeList2 = TypeList;
    while (*TypeList2) {
      strcpy( Type, TypeList2);
      if (x = strchr( Type, ',')) {
        *x = 0;
        TypeList2++;
      }
      TypeList2 += strlen( Type);
      strcpy( Value, ValueList);
      if (x = strchr( Value, ',')) {
        *x = 0;
        ValueList++;
        ValueList += strlen( Value);
      }
      sprintf( Path, "/sys/class/leds/%s%s/%s", Type, Index, Function);
      #ifdef BUGGERS
        printf( "  '%s' = '%s'\n", Path, Value);
      #endif
      if (f = fopen( Path, "w")) {
        fprintf( f, "%s", Value);
        fclose( f);
      } else {
        fprintf( stderr, "*** SetLeds: Bad path '%s'!", Path);
        GlobalReturnCode = 31;
    } }
    if (Stagger_ms) {
      #ifdef BUGGERS
        printf( "    usleep( %d)\n", Stagger_ms);
      #endif
      usleep( Stagger_ms * 1000);
} } }

//--------------------------------------------------------------------------------------------------
// Return a pointer to a static ASCIIZ representation of a 32-bit unsigned decimal integer.
//--------------------------------------------------------------------------------------------------

static ccptr IntStr( uint32_t x) {
  static char s[64];
  sprintf( s, "%d", x);
  return s;
}

//--------------------------------------------------------------------------------------------------
// Set a signal strength pattern on the signal leds.
//--------------------------------------------------------------------------------------------------

static void SetStrength( cptr Value) {
  if (*Value == '-') {
    *Value = '0';
  }
  PutString( FILE_SIG, Value);
  if (PatternActive( PATTERN_SIG) == false) {
    uint32_t i;
    for (i = 5; i >= 1; i--) {
      if (*Value < '0'+i) {
        SetLeds( "sig", IntStr(i), "brightness", "0", 0);
    } }
    for (i = 1; i <= 5; i++) {
      if (*Value >= '0'+i) {
        SetLeds( "sig", IntStr(i), "brightness", "1", 0);
} } } }

//--------------------------------------------------------------------------------------------------
// Set an error pattern on the signal leds.
//--------------------------------------------------------------------------------------------------

static void SetError( cptr Value) {
  PutString( FILE_ERR, Value);
  if (PatternActive( PATTERN_SIG) == false) {
    if ((*Value == 0) || (*Value == '-')) {
      SetStrength( GetString( FILE_SIG));
    } else {
      int errval = atoi( Value);
      SetLeds( "sig", "1", "brightness", (errval & 0x01) ? "1" : "0", 0);
      SetLeds( "sig", "2", "brightness", (errval & 0x02) ? "1" : "0", 0);
      SetLeds( "sig", "3", "brightness", (errval & 0x04) ? "1" : "0", 0);
      SetLeds( "sig", "4", "brightness", (errval & 0x08) ? "1" : "0", 0);
      SetLeds( "sig", "5", "brightness",                   "1"      , 0);
} } }

//--------------------------------------------------------------------------------------------------
// Set the color of one of the rgb leds (usb or gen/3g).
//--------------------------------------------------------------------------------------------------

static void SetRgb( ccptr led, char Value) {
  switch (Value) {
    case 'w': SetLeds( led, "red,green,blue", "brightness", "1,1,1", 0); break;
    case 'y': SetLeds( led, "red,green,blue", "brightness", "1,1,0", 0); break;
    case 'p': SetLeds( led, "red,green,blue", "brightness", "1,0,1", 0); break;
    case 'r': SetLeds( led, "red,green,blue", "brightness", "1,0,0", 0); break;
    case 'a': SetLeds( led, "red,green,blue", "brightness", "0,1,1", 0); break;
    case 'g': SetLeds( led, "red,green,blue", "brightness", "0,1,0", 0); break;
    case 'b': SetLeds( led, "red,green,blue", "brightness", "0,0,1", 0); break;
    default : SetLeds( led, "red,green,blue", "brightness", "0,0,0", 0);
} }

//--------------------------------------------------------------------------------------------------
// Set the color of the usb led.
//--------------------------------------------------------------------------------------------------

static void SetUsb( ccptr Value) {
  PutString( FILE_USB, Value);
  if (PatternActive( PATTERN_USB) == false) {
    SetRgb( "usb_", *Value);
} }

//--------------------------------------------------------------------------------------------------
// Set the color of the gen/3g led.
//--------------------------------------------------------------------------------------------------

static void SetGen( ccptr Value) {
  PutString( FILE_GEN, Value);
  if (PatternActive( PATTERN_GEN) == false) {
    SetRgb( "3g_", *Value);
} }

//--------------------------------------------------------------------------------------------------
// Reduce a sequence of rgb colors to a sequence of one primary color component.
//--------------------------------------------------------------------------------------------------

static ccptr SliceRgb( uint32_t Filter, ccptr ColorSequence, cptr Slice) {
  while (*ColorSequence) {
    uint32_t Rgb;
    switch (*ColorSequence++) {
      case 'w': Rgb = 0x111; break;
      case 'y': Rgb = 0x110; break;
      case 'p': Rgb = 0x101; break;
      case 'r': Rgb = 0x100; break;
      case 'a': Rgb = 0x011; break;
      case 'g': Rgb = 0x010; break;
      case 'b': Rgb = 0x001; break;
      default : Rgb = 0x000;
    }
    *Slice++ = (Rgb & Filter) ? '1' : '-';
  }
  *Slice = 0;
}

//--------------------------------------------------------------------------------------------------
// Return true if the indicated slice can be implemented with a delay/on/off timer.
//--------------------------------------------------------------------------------------------------

static bool ValidSlice( ccptr Slice) {
  #ifdef BUGGERS
    printf( "Slice = '%s'\n", Slice);
  #endif
  if (strcmp( Slice, "--") == 0) return true; // off
  if (strcmp( Slice, "1-") == 0) return true; // off 500,            on 500
  if (strcmp( Slice, "-1") == 0) return true; // off 500, delay 500, on 500
  if (strcmp( Slice, "11") == 0) return true; //                     on

  if (strcmp( Slice, "----") == 0) return true; // off
  if (strcmp( Slice, "1---") == 0) return true; // off 750           on 250
  if (strcmp( Slice, "-1--") == 0) return true; // off 750 delay 250 on 250
  if (strcmp( Slice, "--1-") == 0) return true; // off 750 delay 500 on 250
  if (strcmp( Slice, "---1") == 0) return true; // off 750 delay 750 on 250
  if (strcmp( Slice, "11--") == 0) return true; // off 500           on 500
  if (strcmp( Slice, "-11-") == 0) return true; // off 500 delay 250 on 500
  if (strcmp( Slice, "--11") == 0) return true; // off 500 delay 500 on 500
  if (strcmp( Slice, "111-") == 0) return true; // off 250           on 750
  if (strcmp( Slice, "-111") == 0) return true; // off 250 delay 250 on 750
  if (strcmp( Slice, "1111") == 0) return true; //                   on

  if (strcmp( Slice, "1-1-") == 0) return true; // off 250           on 250
  if (strcmp( Slice, "-1-1") == 0) return true; // off 250 delay 250 on 250

  return false;
}

//--------------------------------------------------------------------------------------------------
// Implement a blinking or solid on/off primary color component of an rgb led.  Handle a couple of
// special cases such as '--', '11, '----', '1111', '-1-1' and '1-1-'.
//--------------------------------------------------------------------------------------------------

static bool DoColorPat( ccptr Led, ccptr Color, ccptr Pattern, uint32_t TimeSlice) {
  uint32_t On = 0, Off = 1000, Total = 0;
  ccptr Cursor = Pattern;
  while (*Cursor) {
    Total += TimeSlice;
    if (*Cursor == '1') {
      On  += TimeSlice;
      Off -= TimeSlice;
    }
    Cursor++;
  }
  if (Total == 1000) {
    if (Off == 1000) { // pattern is all '0'
      SetLeds( Led, Color, "brightness", "0"   , 0);
      SetLeds( Led, Color, "trigger"   , "none", 0);
      return true;
    }
    if (On == 1000) { // pattern is all '1'
      SetLeds( Led, Color, "brightness", "1"   , 0);
      SetLeds( Led, Color, "trigger"   , "none", 0);
      return true;
  } }
  if (*Pattern == '1') {
    if (strncmp( Pattern, "1-1", 3) == 0) {
      Off /= 2;
      On  /= 2;
    }
    SetLeds( Led, Color, "brightness", "0"        , 0); // gen dark
    SetLeds( Led, Color, "trigger"   , "timer"    , 0); // gen timer
    SetLeds( Led, Color, "delay_off" , IntStr(Off), 0); // gen off
    SetLeds( Led, Color, "delay_on"  , IntStr(On ), 0); // gen on
    return true;
  }
  return false;
}

//--------------------------------------------------------------------------------------------------
// Carve up a sequence of colors into its constituent red/green/blue slices and verify that each
// of the slices can be implemented.  If all looks good, make it so!
//--------------------------------------------------------------------------------------------------

static void DoGenPattern( ccptr Pattern) {
  char  r[8]      , g[8]      , b[8]      ;
  ccptr rp = r    , gp = g    , bp = b    ;
  bool  rx = false, gx = false, bx = false;
  uint32_t i, TimeSlice_ms = 0;
  SliceRgb( 0x100, Pattern, r);
  SliceRgb( 0x010, Pattern, g);
  SliceRgb( 0x001, Pattern, b);
  if (!ValidSlice( r)) { fprintf( stderr, "*** DoGenPattern: Pattern '%s' [r] can not be implemented!\n", Pattern); GlobalReturnCode = 41; return; }
  if (!ValidSlice( g)) { fprintf( stderr, "*** DoGenPattern: Pattern '%s' [g] can not be implemented!\n", Pattern); GlobalReturnCode = 42; return; }
  if (!ValidSlice( b)) { fprintf( stderr, "*** DoGenPattern: Pattern '%s' [b] can not be implemented!\n", Pattern); GlobalReturnCode = 43; return; }
  if (strlen( Pattern) == 2) TimeSlice_ms = 500;
  if (strlen( Pattern) == 4) TimeSlice_ms = 250;
  if (TimeSlice_ms) {
    for (i = 0; i < strlen( Pattern); i++) {
      if (rx == false) rx = DoColorPat( "3g_", "red"  , rp, TimeSlice_ms); rp++;
      if (gx == false) gx = DoColorPat( "3g_", "green", gp, TimeSlice_ms); gp++;
      if (bx == false) bx = DoColorPat( "3g_", "blue" , bp, TimeSlice_ms); bp++;
      if ((rx == false) || (gx == false) || (bx == false)) {
        #ifdef BUGGERS
          printf( "    usleep( %d)\n", TimeSlice_ms);
        #endif
        usleep( TimeSlice_ms * 1000);
} } } }

//--------------------------------------------------------------------------------------------------
// Return true if all the colors in the sequence are valid.
//--------------------------------------------------------------------------------------------------

static bool CheckColors( ccptr Sequence) {
  while (*Sequence) {
    if (!strchr( COLORS, *Sequence)) {
      fprintf( stderr, "*** CheckColors: Unexpected color '%c'!  Choose colors from '%s'.\n", *Sequence, COLORS);
      GlobalReturnCode = 51;
      return false;
    }
    Sequence++;
  }
  return true;
}

//--------------------------------------------------------------------------------------------------
// Patterns are:
//
//   -/off . . . . . . . . all patterns terminated
//   cylon/download  . . . signal lights cycle
//   busy/reconfigure  . . gen/usb lights cycle red/green
//   2xy . . . . . . . . . gen light 2 x 500ms colors
//   4xxyy . . . . . . . . gen light 4 x 250ms colors (same as 2xy)
//   4wzyz . . . . . . . . gen light 4 x 250ms colors
//
// You may combine download/cylon and any one of the other patterns.
// Named patterns may be abbreviated down to 3 characters.
//
// For the 2 x 250ms colors they must resolve to simple patterns for
// each of the three primary colors (r/g/b) that consist of a nonzero
// on period (500/1000ms) followed by an off period which fills out a
// 1000ms cycle.
//
// For the 4 x 250ms colors they must resolve to simple patterns for
// each of the three primary colors (r/g/b) that consist of a nonzero
// on period (250/500/750/1000ms) followed by an off period which fills
// out either a 500ms or 1000ms cycle.
//--------------------------------------------------------------------------------------------------

static void SetPattern( cptr Pattern) {
  bool Handled = false;
  cptr ColorSequence = Pattern+1;

  // Terminate all patterns and restore static led values.

  if ((strcmp( Pattern, "-"  ) == 0) ||
      (strcmp( Pattern, "off") == 0)) {
    Handled = true;
    if (PatternActive( PATTERN_SIG)) {
      SetLeds( "sig", "5,4,3,2,1", "trigger", "none", 0);
      PatternTerminate( PATTERN_SIG);
      SetError( GetString( FILE_ERR)); // also handles SetStrength()
    }
    if (PatternActive( PATTERN_USB)) {
      SetLeds( "usb_", "red,green,blue", "trigger", "none", 0);
      PatternTerminate( PATTERN_USB);
      SetUsb( GetString( FILE_USB));
    }
    if (PatternActive( PATTERN_GEN)) {
      SetLeds( "3g_", "red,green,blue", "trigger", "none", 0);
      PatternTerminate( PATTERN_GEN);
      SetGen( GetString( FILE_GEN));
  } }

  // Cyclic pattern on signal leds.

  if (strncmp( Pattern, "cyl", 3) == 0 || // cylon
      strncmp( Pattern, "dow", 3) == 0) { // download
    Handled = true;
    PatternInitiate( PATTERN_SIG);
    SetLeds( "sig", "5,4,3,2,1", "brightness", "0"    ,  0); // sigs dark
    SetLeds( "sig", "5,4,3,2,1", "trigger"   , "timer",  0); // sigs timer
    SetLeds( "sig", "5,4,3,2,1", "delay_off" , "535"  ,  0); // sigs off=535
    SetLeds( "sig", "5,4,3,2,1", "delay_on"  , "100"  , 33); // sigs on=100, 33 stagger
  }

  // Alternating red/green pattern on usb/gen leds.

  if ((strncmp( Pattern, "bus", 3) == 0) || // busy
      (strncmp( Pattern, "rec", 3) == 0)) { // reconfigure
    Handled = true;
    if (PatternActive( PATTERN_USB) || PatternActive( PATTERN_GEN)) {
      fprintf( stderr, "*** SetPattern: Cancel current pattern and retry!\n");
      GlobalReturnCode = 61;
    } else {
      PatternInitiate( PATTERN_USB);
      PatternInitiate( PATTERN_GEN);
      SetLeds( "usb_,3g_", "red,green,blue", "brightness", "0"    ,   0); // usb/gen dark
      SetLeds( "usb_,3g_", "red,green"     , "trigger"   , "timer",   0); // usb/gen red/green timer
      SetLeds( "usb_,3g_", "blue"          , "trigger"   , "none" ,   0); // usb/gen blue none
      SetLeds( "usb_,3g_", "red,green"     , "delay_off" , "500"  ,   0); // usb/gen red/green off=500
      SetLeds( "usb_"    , "green"         , "delay_on"  , "500"  ,   0); // usb green on=500
      SetLeds( "3g_"     , "red"           , "delay_on"  , "500"  , 500); // gen red on=500, wait 500
      SetLeds( "usb_"    , "red"           , "delay_on"  , "500"  ,   0); // usb red on=500
      SetLeds( "3g_"     , "green"         , "delay_on"  , "500"  ,   0); // gen green on=500
  } }

  // Handle patterns of the form '4yyzz' and '4wxyz' on the gen led.  This form specifies 4 colors
  // for 250ms each.  For the special case of the '4yyzz' patterns, recast as a '2yz' pattern.

  if ((*Pattern == '4') && (strlen( Pattern) == 5)) {
    if (CheckColors( ColorSequence)) {
      if ((ColorSequence[0] == ColorSequence[1]) &&
          (ColorSequence[2] == ColorSequence[3])) {
        Pattern[0] = '2';
        ColorSequence[1] = ColorSequence[2];
        ColorSequence[2] = 0;
      } else {
        Handled = true;
        if (PatternActive( PATTERN_USB) || PatternActive( PATTERN_GEN)) {
          fprintf( stderr, "*** SetPattern: Cancel current pattern and retry!\n");
          GlobalReturnCode = 62;
        } else {
          PatternInitiate( PATTERN_GEN);
          DoGenPattern( ColorSequence);
  } } } }

  // Handle patterns of the form '2yz' on the gen led.  This form specifies 2 colors
  // for 500ms each.

  if ((*Pattern == '2') && (strlen( Pattern) == 3)) {
    if (CheckColors( ColorSequence)) {
      Handled = true;
      if (PatternActive( PATTERN_USB) || PatternActive( PATTERN_GEN)) {
        fprintf( stderr, "*** SetPattern: Cancel current pattern and retry!\n");
        GlobalReturnCode = 63;
      } else {
        PatternInitiate( PATTERN_GEN);
        DoGenPattern( ColorSequence);
  } } }

  // Nothing matched!

  if (Handled == false) {
    fprintf( stderr, "*** SetPattern: Pattern '%s' not recognized!\n", Pattern);
    GlobalReturnCode = 64;
} }

//--------------------------------------------------------------------------------------------------
// Display version and usage summary.
//--------------------------------------------------------------------------------------------------

static void ShowHelp( void) {
  printf(
    "\n"
    PROGRAM " " VERSION " - This tool is used to set the state of GPIO pins on the NetBridge-FX.\n"
    "\n"
    "Usage:\n"
    "\n"
    "  gpio {option}\n"
    "\n"
    "Where {option} is one of:\n"
    "\n"
    "  -u c, --usb=c\n"
    "    Set the USB LED color (see COLORS below)\n"
    "  -u N, --usb=N\n"
    "    Enable or disable usb ports (N=0:both off, 1:both on, 2:internal on, 3:external on)\n"
    "  -g c, --gen=c, --3g=c\n"
    "    Set the GEN LED color (see COLORS below)\n"
    "  -s N, --sig=N\n"
    "    Set the signal strength bars to level N (0..5)\n"
    "  -r N, --err=N\n"
    "    Set the signal strength bars to error N (1..15)\n"
    "  -p P, --pat=P\n"
    "    Set the pattern to: off, cylon/download, busy/reconfigure, 2yz, 4wxyz\n"
    "  -e N, --eth=N\n"
    "    Enable or disable the Ethernet port (N=0 to disable, N=1 to enable)\n"
    "\n"
    "Color codes for usb/gen LEDs and patterns:\n"
    "    r=red, g=green, b=blue, y=yellow, a=aqua, p=purple, w=white, -=off\n"
    "\n");
  GlobalReturnCode = 71;
}

//--------------------------------------------------------------------------------------------------
// Accept a single command-line argument which specifies an led or gpio configuration.
//--------------------------------------------------------------------------------------------------

int main( int argc, char ** argv){
  int c, option_index = 0;

  struct timespec SemTs;
  if (clock_gettime( CLOCK_REALTIME, &SemTs) != 0){
    fprintf( stderr, "*** gpio: error setting timeout: %d\n", errno);
    GlobalReturnCode = 1;
  }

  sem_t * Sem = NULL;
  if (GlobalReturnCode == 0) {
    SemTs.tv_sec += 10;
    Sem = sem_open( PROGRAM, O_CREAT, S_IRUSR | S_IWUSR, 1);
    if (Sem == SEM_FAILED) {
      fprintf( stderr, "*** gpio: can't open lock '%s': %d\n", PROGRAM, errno);
      GlobalReturnCode = 2;
  } }

  if (GlobalReturnCode == 0) {
    if (sem_timedwait( Sem, &SemTs) != 0){
      if (errno == ETIMEDOUT) {
        fprintf( stderr, "*** gpio: timed out waiting for lock\n");
        int SemVal;
        sem_getvalue( Sem, &SemVal);
        fprintf( stderr, "*** gpio: Forcing semaphore reset. Value was: %d\n", SemVal);
        sem_unlink( PROGRAM);
        sem_close( Sem);
        GlobalReturnCode = 3;
      } else {
        fprintf( stderr, "*** gpio: could not get lock\n");
        GlobalReturnCode = 4;
      }
      GlobalReturnCode = 5;
  } }

  static struct option long_options[] = {
    { "eth" , required_argument, NULL, 'e' },
    { "sig" , required_argument, NULL, 's' },
    { "err" , required_argument, NULL, 'r' },
    { "usb" , required_argument, NULL, 'u' },
    { "3g"  , required_argument, NULL, 'g' },
    { "gen" , required_argument, NULL, 'g' },
    { "pat" , required_argument, NULL, 'p' },
  };

  if (GlobalReturnCode == 0) {
    if ((c = getopt_long( argc, argv, "e:s:r:u:g:p:", long_options, &option_index)) == -1) {
      ShowHelp();
    } else {
      switch (c) {
        case 'e':
          SetGpio( GPIO_ETH_EN, optarg);
          break;
        case 's':
          SetStrength( optarg);
          break;
        case 'r':
          SetError( optarg);
          break;
        case 'u':
          if (strcmp( optarg, "0") == 0 || strcmp( optarg, "1") == 0) {
            SetGpio( GPIO_USB_EXT_EN, optarg);
            SetGpio( GPIO_USB_INT_EN, optarg);
          } else {
            if (strcmp( optarg, "2") == 0) {
              SetGpio( GPIO_USB_EXT_EN, "0");
              SetGpio( GPIO_USB_INT_EN, "1");
            } else {
              if (strcmp( optarg, "3") == 0) {
                SetGpio( GPIO_USB_EXT_EN, "1");
                SetGpio( GPIO_USB_INT_EN, "0");
              } else {
                SetUsb( optarg);
          } } }
          break;
        case 'g':
          SetGen( optarg);
          break;
        case 'p':
          SetPattern( optarg);
          break;
        default:
          ShowHelp();
  } } }

  sem_post( Sem);
  return GlobalReturnCode;
}

//-----------------------------------------------------------------------------
// End
//-----------------------------------------------------------------------------

